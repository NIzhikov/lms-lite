package test


import scala.virtualization.lms.{JavaLoopsPackage, LoopsAPI}

/**
 * @author NIzhikov
 */
class JavaLoopsSuite extends LMSSuite {
  def countLoops(str1: String): Int = {
    def count(pos: Int, c: Int): Int = {
      val idx = str1.indexOf("for", pos)
      if (idx == -1) c else count(idx + 1, c + 1)
    }
    count(0, 0)
  }

  test("array implementation") {
    trait P extends LoopsAPI {
      def test(in: Rep[Int]): Rep[Array[Int]] = {
        array(in, i => i)
      }
    }
    val sProg = new P with JavaLoopsPackage
    val fProg = new P with JavaLoopsPackage {
      fuse = true
    }

    debug(emitSourceJava(sProg)(sProg.test))
    val s = sProg.compileJava(sProg.test)
    assert(s(5) === Array(0, 1, 2, 3, 4))

    debug(emitSourceJava(fProg)(fProg.test))
    val f = fProg.compileJava(fProg.test)
    assert(f(5) === Array(0, 1, 2, 3, 4))
  }

  test("arrayIf implementation") {
    trait P extends LoopsAPI {
      def test(in: Rep[Int]): Rep[Array[Int]] = {
        arrayIf(in, i => i, i => i > 2)
      }
    }
    val sProg = new P with JavaLoopsPackage
    val fProg = new P with JavaLoopsPackage {
      fuse = true
    }
    debug(emitSourceJava(sProg)(sProg.test))
    val s = sProg.compileJava(sProg.test)
    assert(s(5) === Array(3, 4))

    debug(emitSourceJava(fProg)(fProg.test))
    val f = fProg.compileJava(fProg.test)
    assert(f(5) === Array(3, 4))
  }

  test("reduce implementation") {
    trait P extends LoopsAPI {
      def test(in: Rep[Int]): Rep[Int] = {
        reduceLoop(
          in,
          func = i => i,
          red = (x: Rep[Int], y: Rep[Int]) => x + y,
          zero = 0
        )
      }
    }
    val sProg = new P with JavaLoopsPackage
    val fProg = new P with JavaLoopsPackage {fuse = true}

    debug(emitSourceJava(sProg)(sProg.test))
    val s = sProg.compileJava(sProg.test)
    assert(s(4) === 6)

    debug(emitSourceJava(fProg)(fProg.test))
    val f = fProg.compileJava(fProg.test)
    assert(f(4) === 6)
  }

  test("Horizontal fusion") {
    trait P extends LoopsAPI {
      def test(in: Rep[Int]): Rep[Array[Int]] = {
        val a1 = arrayIf(in, i => i, i => i > 2)
        val a2 = array(a1.length, i => a1.at(i) + 1)
        a2
      }
    }
    val sProg = new P with JavaLoopsPackage
    val fProg = new P with JavaLoopsPackage {fuse = true}

    val noFusionJava = emitSourceJava(sProg)(sProg.test)
    debug("no fusion")
    debug(noFusionJava)

    val s = sProg.compileJava(sProg.test)
    assert(s(5) === Array(4, 5))
    assert(2 === countLoops(noFusionJava))

    val fusedJava = emitSourceJava(fProg)(fProg.test)
    debug("fused")
    debug(fusedJava)
    val f = fProg.compileJava(fProg.test)
    assert(f(5) === Array(4, 5))
    assert(1 === countLoops(fusedJava))
  }

  // collect and then reduce are fused
  test("Vertical fusion") {
    trait P extends LoopsAPI {
      def test(in: Rep[Array[Int]]): Rep[Int] = {
        val ar1: Rep[Array[Int]] = array(in.length, i => in.at(i))

        reduceLoop(
          ar1.length,
          func = i => ar1.at(i),
          red = (x: Rep[Int], y: Rep[Int]) => x + y,
          zero = 0
        )
      }
    }

    val sProg = new P with JavaLoopsPackage
    val fProg = new P with JavaLoopsPackage {fuse = true}

    val noFusionJava = emitSourceJava(sProg)(sProg.test)
    debug("no fusion")
    debug(noFusionJava)

    val s:j.JFunction1[Array[Integer], Int] = sProg.compileJava(sProg.test).asInstanceOf[j.JFunction1[Array[Integer], Int]]
    assert(s.apply(Array(1,2,3)) === 6)
    assert(2 === countLoops(noFusionJava))

    val fusedJava = emitSourceJava(fProg)(fProg.test)
    debug("fused")
    debug(fusedJava)
    val f:j.JFunction1[Array[Integer], Int] = fProg.compileJava(fProg.test).asInstanceOf[j.JFunction1[Array[Integer], Int]]
    assert(f(Array(1,2,3)) === 6)
    assert(1 === countLoops(fusedJava))
  }
}
