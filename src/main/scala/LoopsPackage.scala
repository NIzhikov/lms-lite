package scala.virtualization.lms

import common._
import scala.virtualization.lms.internal.{JavaCompile, ScalaCompile}

trait LoopsAPI
    extends IfThenElse
    with Functions
    with PrimitiveOps
    with BooleanOps
    with NumericOps
    with LiftNumeric
    with ArrayLoops
    with ArrayLoopsOps
    with OrderingOps

trait LoopsScalaCodegen
    extends ScalaGenBase
    with ScalaGenBooleanOps
    with ScalaGenFunctions
    with ScalaGenIfThenElseFat
    with ScalaGenFatArrayLoopsFusionOpt
    with ScalaGenNumericOps
    with ScalaGenOrderingOps {
    val IR: LoopsPackage
}

trait LoopsJavaCodegen
    extends JavaGenBase
    with JavaGenBooleanOps
    with JavaGenFunctions
    with JavaGenIfThenElseFat
    with JavaGenFatArrayLoopsFusionOpt
    with JavaGenNumericOps
    with JavaGenOrderingOps {
        val IR: JavaLoopsPackage
    }

trait BaseLoopsPackage
    extends IfThenElseExp
    with FunctionsRecursiveExp
    with PrimitiveOpsExp
    with BooleanOpsExp
    with NumericOpsExp
    with IfThenElseFatExp
    with ArrayLoopsFatExp
    with OrderingOpsExp

trait LoopsPackage
    extends BaseLoopsPackage
    with ScalaCompile { self =>
        var fuse = false

        val scalaCodegen = new LoopsScalaCodegen {
            val IR: self.type = self

            override def shouldApplyFusion(currentScope: List[Stm])(result: List[Exp[Any]]): Boolean = fuse
        }
    }

trait JavaLoopsPackage
    extends BaseLoopsPackage
    with JavaCompile { self =>
        var fuse = false

        val javaCodegen = new LoopsJavaCodegen {
            val IR: self.type = self

            override def shouldApplyFusion(currentScope: List[Stm])(result: List[Exp[Any]]): Boolean = fuse
        }
    }
