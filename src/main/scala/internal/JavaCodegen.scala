package scala.virtualization.lms
package internal

import scala.reflect.{RefinedManifest}
import scala.collection.mutable.Stack
import scala.collection.parallel.mutable


/**
 * @author NIzhikov
 */
trait JavaCodegen extends GenericCodegen with Config {
  val IR: Expressions

  import IR._

  override def kernelFileExt = "java"

  override def toString = "java"

  def emitTopDef[A: Manifest](args: List[Sym[_]], body: Block[A], className: String, out: java.io.PrintWriter) = {
    val sA = remap(manifest[A], true)
    val returnSuffix =  if (sA.indexOf("[]") != -1) {
      s".toArray(new ${sA.substring(0, sA.indexOf("[]"))}[0]);"
    } else {
      ";"
    }

    withStream(out) {
      stream.println()
      stream.println(s"public class ${className} implements j.JFunction${args.size}<${args.map(a => remap(a.tp, true)).mkString(", ")}, ${sA}>, java.io.Serializable {")
      stream.println(s"public ${sA} apply(final ${args.map(a => remap(a.tp, true) + " " + quote(a)).mkString(", ")}) {")
      args.foreach{a =>
        val t = remap(a.tp, true)
        if (t.indexOf("[]") != -1) {
          val newSym =  fresh[Array[Int]]//TODO make common definition
          emitValDef(newSym, s"new java.util.ArrayList<java.lang.Integer>()")
          stream.println(s"${quote(newSym)}.addAll(java.util.Arrays.asList(${quote(a)}));")
          replaces += a -> newSym
        }
      }

      emitBlock(body)
      stream.println("return " + quote(getBlockResult(body)) + returnSuffix)

      stream.println("}")

      stream.println("}")
    }
  }

  def relativePath(fileName: String): String = {
    val i = fileName.lastIndexOf('/')
    fileName.substring(i + 1)
  }

  def emitValDef(sym: Sym[Any], rhs: String): Unit = {
    stream.println(remap(sym.tp) + " " + quote(sym) + " = " + rhs + ";")
  }

  def emitAssignment(lhs: String, rhs: String): Unit = {
    stream.println(lhs + " = " + rhs + ";")
  }

  override def remap(s: String): String = {
    s match {
      case "Any" => "java.lang.Object"
      case "Int" => "java.lang.Integer"
      case "Array" => "java.util.ArrayList"
      case "scala.Function1" => "j.JFunction1"
      case "Unit" => "void"
      case _ => s
    }
  }

  override def remap[A](s: String, method: String, t: Manifest[A]): String = remap(s, method, t.toString)

  override def remap(s: String, method: String, t: String): String = s + method + "<" + remap(t) + ">"

  override def remap[A](m: Manifest[A]): String = remap(m, false)

  def remap[A](m: Manifest[A], external: Boolean): String = {
    m match {
      case rm: RefinedManifest[A] => "{" + rm.fields.foldLeft("") {
        (acc, f) => {
          val (n, mnf) = f; acc + remap(mnf) + " " + n + ";"
        }
      } + "}"
      case _ =>
        // call remap on all type arguments
        val targs = m.typeArguments
        val ms = m.toString.replaceAll("\\[", "<").replaceAll("\\]", ">")
        val mainType = ms.take(ms.indexOf("<"))

        if (external && mainType == "Array") {
          if (targs.length == 1) {
            remap(targs.head) + "[]"
          } else
              throw new GenerationFailedException("don't know how to generate code for external array")
        } else if (targs.length > 0) {
          remap(mainType) + "<" + targs.map(tp => remap(tp)).mkString(", ") + ">"
        } else remap(m.toString)
    }
  }
}

trait JavaNestedCodegen extends GenericNestedCodegen with JavaCodegen {
    val IR: Blocks
    import IR._

    // emit forward decls for recursive vals
    def emitForwardDef(sym: Sym[Any]): Unit = {
        stream.println(remap(sym.tp) + " " + quote(sym) + " = (" + remap(sym.tp) + ")null")
    }

    // special case for recursive vals
    override def emitValDef(sym: Sym[Any], rhs: String): Unit = {
        if (recursive contains sym)
            stream.println(s"${remap(sym.tp)} ${quote(sym)} = " + rhs + ";") // we have a forward declaration above.
        else
            super.emitValDef(sym,rhs)
    }
}

trait JavaFatCodegen extends GenericFatCodegen with JavaCodegen {
    val IR: Blocks with FatExpressions
}
