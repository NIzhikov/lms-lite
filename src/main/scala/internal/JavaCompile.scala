package scala.virtualization.lms
package internal

import j.{JFunction1, JFunction2}
import java.io.{File, PrintWriter, StringWriter}
import javax.tools.ToolProvider
import java.nio.charset.Charset
import scala.collection.JavaConverters._

import scala.tools.nsc.io._

import scala.tools.nsc.interpreter.AbstractFileClassLoader

object JavaCompile {
  var compileCount = 0
}

// The main task to implement for candidate
trait JavaCompile extends Expressions {
  val javaCodegen: JavaCodegen {val IR: JavaCompile.this.type}

  var dumpGeneratedCode = false

  val compiler = ToolProvider.getSystemJavaCompiler

  def compile(source: String, className: String): Class[_] = {
    val fileManager = compiler.getStandardFileManager(null, null, Charset.forName("UTF-8"))
    try {
      val outputDir = {
        val tmp = System.getProperty("java.io.tmpdir")
        val dir = new File(tmp + File.separator + "lmsoutput")
        dir.mkdir()
        dir
      }
      val file2compile = {
        val f = new File(outputDir.getPath, className + ".java")
        if (f.exists())
          f.delete()
        val c = new File(outputDir.getPath, className + ".class")
        if (c.exists())
          c.delete()
        f.deleteOnExit()
        Some(new PrintWriter(f)).foreach {p => p.write(source); p.close()}
        f
      }

      val compilationUnits = fileManager.getJavaFileObjects(file2compile)
      val options = List("-classpath", this.getClass.getClassLoader match {
        case ctx: java.net.URLClassLoader => ctx.getURLs.map(_.getPath).mkString(File.pathSeparator)
        case _ => System.getProperty("java.class.path")
      }, "-d", outputDir.getPath)
      val compileTask = compiler.getTask(null, fileManager, null, options.asJava, null, compilationUnits)

      if (!compileTask.call) {
        println(source)
        println("compilation: had errors")
      }

      val parent = this.getClass.getClassLoader
      val loader = new AbstractFileClassLoader(AbstractFile.getDirectory(outputDir), this.getClass.getClassLoader)
      loader.loadClass(className)
    } finally {
      fileManager.close
    }
  }

  // Generates java code for function of one argument. Standard javac should be able to compile this code.
  def emitSourceJava[A, B](f: Exp[A] => Exp[B], className: String)(implicit mA: Manifest[A], mB: Manifest[B]): String = {
    val source = new StringWriter()
    javaCodegen.emitSource(f, className, new PrintWriter(source))
    source.toString
  }

  // Generates java code for function of two arguments. Standard javac should be able to compile this code.
  def emitSourceJava2[A, B, C](f: (Exp[A], Exp[B]) => Exp[C], className: String)(implicit mA: Manifest[A], mB: Manifest[B], mC: Manifest[C]): String = {
    val source = new StringWriter()
    javaCodegen.emitSource2(f, className, new PrintWriter(source))
    source.toString
  }

  // Instantiates compiled code
  def compileJava[A, B](f: Exp[A] => Exp[B])(implicit mA: Manifest[A], mB: Manifest[B]): JFunction1[A, B] = {
    import JavaCompile._
    val className = "StagedJ" + compileCount
    try {
      val source = emitSourceJava(f, className)
      if (dumpGeneratedCode) println(source)

      compile(source, className).newInstance().asInstanceOf[JFunction1[A, B]]
    } finally {
      compileCount += 1
    }
  }

  // Instantiates compiled code
  def compileJava2[A, B, C](f: (Exp[A], Exp[B]) => Exp[C])(implicit mA: Manifest[A], mB: Manifest[B], mC: Manifest[C]): JFunction2[A, B, C] = {
    import JavaCompile._
    val className = "StagedJ" + compileCount
    try {
      val source = emitSourceJava2(f, className)
      if (dumpGeneratedCode) println(source)

      compile(source, className).newInstance().asInstanceOf[JFunction2[A, B, C]]
    } finally {
      compileCount += 1
    }
  }
}
